+++
title = "License"
date =  2021-10-09T10:51:52+02:00
weight = 10
+++
![Apache License, version 2.0](https://img.shields.io/badge/license-Apache%202-blue)

The core projects of **Intutable** are licensed under the [Apache 2](https://www.apache.org/licenses/LICENSE-2.0) license.  

Plugins can be licensed with any license you like as long as they are compatible with the Apache 2 license.
Keep in mind if you want to have your plugin listed on this page, it has to be open source.
