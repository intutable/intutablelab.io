+++
title = "Contributing"
date =  2021-10-09T10:51:33+02:00
weight = 8
+++

**Intutable** is and always will be an open source project. 
Therefore, the project follows general open source and development guidelines.

### Contributing guidelines
If you want to contribute to the core projects, you are free to fork the repositories
and open a merge request. Keep in mind, that it may take some time until you get
feedback on your merge request. 

If you want to create a new plugin, take a look at the Plugin development section.
Contributing to third party projects is not covered here, instead you have to get in touch with the plugin author.

If you want to contribute to the core projects, make sure to follow our development guidelines.

### Development guidelines
#### Commit guidelines
Commits should follow the following naming scheme and if possible not longer than 50 characters:
**TYPE(SCOPE): COMMIT MESSAGE**

Types can be:
- **fix**: bugfixes
- **feat**: new feature implementation
- **chore**: everything which can't be categorized, e.g license or README changes
- **refactor**: code style, typos, moving code
- **ci**: commits regarding CI pipelines
- **docs**: commits regarding documentation
- **test**: commits regarding tests

The scope is the corresponding module you are working on.
Note that the scope is optional, but should be included whenever possible.

Examples:
- feat(student): add age attribute
- feat(teacher): add letFailCourse method
- fix(school): make number of students unsigned
- chore(license): change license to apache2
- ci(fix): don’t release before building
- test(student): add tests for name changing
- refactor(teacher): fix typo in lastName

### Best practices
If you fork a repository, make sure to do your changes not on the *main* or *develop* branch,
instead create a feature branch. This makes sure you can pull upstream changes in your repository
without running into merge conflicts.

When you create a merge request, make sure to target the upstream *develop* branch. Otherwise your merge request will be rejected.

Make sure to include an accurate description in your merge request.