+++
title = "Intutable"
date =  2021-10-03T17:00:35+02:00
weight = 2
+++

**Intutable** is a multi purpose and easy extendable data wrangling tool written in typescript.  

The development mostly takes place in the [intutable group](https://gitlab.com/intutable) at GitLab.  
**Intutable** is usable with a Web-frontend, CLI or REST-API.

## Example
*SCREENSHOT PLACEHOLDER*