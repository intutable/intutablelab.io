+++
title = "Architecture"
date =  2021-10-03T16:53:43+02:00
weight = 5
+++

**Intutable** consist of one core module and any number of plugins.  
![Example communication](/diagrams/overview.png)


### Core 
The [core](https://gitlab.com/intutable/core) module only has two functions.
1. A plugin system which can load the actual plugins
2. An event system which handles the communication between plugins

The architecture is designed with asynchronous operations in mind.

### Plugins
Plugins can have two forms. Plugins can be plugins **and/or** middleware.

#### Middleware
A middleware is a form of a plugin which listens on all available plugin channels.
Middlewares can:
- add content to a request
- resolve a request
- reject a request
- receive and send notifications

#### Plugins
Plugins on the other side can only
- resolve a request
- reject a request
- receive and send notifications
on their respective subscribed channels.

### Example communication

![Example communication](/diagrams/plugincommunication.png)