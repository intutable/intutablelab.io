+++ 
title = "List of plugins"
date = 2021-10-03T15:01:00+02:00 
weight = 4 
+++
Here you can find a curated list of plugins to extend the functionality of intutable.

| Plugin      | Description | Build Status | Coverage |
| :---        |    :----   | :--- | :---|
| [data-import-export](https://gitlab.com/intutable/data-import-export)| Plugin to import and export data in various formats| ![](https://gitlab.com/intutable/data-import-export/badges/main/pipeline.svg) | ![](https://gitlab.com/intutable/data-import-export/badges/main/coverage.svg) |
| [database](https://gitlab.com/intutable/database)   | Plugin to access a database. **This plugin will be mandatory for most use-cases.** | ![](https://gitlab.com/intutable/database/badges/main/pipeline.svg) | ![](https://gitlab.com/intutable/database/badges/main/coverage.svg) | 
| [example-plugin](https://gitlab.com/intutable/example-plugin)   | Example plugin which can be used as a template to create new plugins | ![](https://gitlab.com/intutable/example-plugin/badges/main/pipeline.svg) | ![](https://gitlab.com/intutable/example-plugin/badges/main/coverage.svg) |
| [http](https://gitlab.com/intutable/http)   | Plugin which exposes channels as REST-Endpoints | ![](https://gitlab.com/intutable/http/badges/main/pipeline.svg) | ![](https://gitlab.com/intutable/http/badges/main/coverage.svg) |
| [sveltekit-gui](https://gitlab.com/intutable/sveltekit-gui)   | A frontend plugin created with sveltekit | ![](https://gitlab.com/intutable/sveltekit-gui/badges/main/pipeline.svg) | ![](https://gitlab.com/intutable/sveltekit-gui/badges/main/coverage.svg) |
| [user-authentication](https://gitlab.com/intutable/user-authentication)   | Plugin to handle user authentication | ![](https://gitlab.com/intutable/user-authentication/badges/main/pipeline.svg) | ![](https://gitlab.com/intutable/user-authentication/badges/main/coverage.svg) |
| [user-permission](https://gitlab.com/intutable/user-permission)   | Plugin to handle permissions for users | ![](https://gitlab.com/intutable/user-permission/badges/main/pipeline.svg) | ![](https://gitlab.com/intutable/user-permission/badges/main/coverage.svg) |


### You want your Plugin listed here?
If you have written a plugin and want it to be displayed in the list on this page, contact the intutable team or create a merge request.