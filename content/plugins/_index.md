+++
title = "Plugins"
date =  2021-10-03T15:00:19+02:00
weight = 3
+++
Plugins are a way to extend the functionality of the intutable project.  
**Plugins are mandatory to implement the actual functionalities.**

### Available plugins
Take a loot at [the plugin list]({{< ref "/plugins/pluginlist" >}}).