+++
title = "How to create a plugin"
date =  2021-10-12T23:53:05+02:00
weight = 5
+++

Creating a new plugin is easy, just follow the steps on this page.  
Be careful when choosing a channel name so that you don't override an existing channel.

You can find [an example project](https://gitlab.com/intutable/example-plugin) on GitLab 
which can be used as a template for a new project.

## Plugin structure
A plugin has to have an init function and can have an optional close function.  
In the init function, the plugin can register its channels and callbacks.  
Code in the close function gets executed, when the application gets stopped.
```typescript
import { PluginLoader, CoreRequest, CoreResponse } from "@intutable/core"

export async function init(plugin: PluginLoader) {
    // Request handling
    plugin
        .listenForRequest("PLUGIN-CHANNEL")
        .on("doStuff", doStuff)
        .on("doOtherStuff", doOtherStuff)

    // Notification handling
    plugin
        .listenForNotifications("PLUGIN-CHANNEL")
        .on("stuffHappened", stuffHappened)
        .on("otherStuffHappend", otherStuffHappened)
}

export async function close() {
    // Optional code that gets executed when stopping the application
}

async function doStuff(request: CoreRequest): Promise<CoreResponse> {
    // do stuff in here on request
    return Promise.resolve({ message: `executed doStuff` })
}

async function doOtherStuff(request: CoreRequest): Promise<CoreResponse> {
    // do stuff in here on request
    return Promise.resolve({message: `executed doOtherStuff`})
}
    
async function stuffHappened(notificaton: CoreNotification) {
    // do stuff on notification        
}

async function otherStuffHappened(notificaton: CoreNotification) {
    // do stuff on notification        
}
```
To send a request to a plugin, you can use the following snippet as an example to call doStuff():
```typescript
import { Core } from "@intutable/core"
let core: Core

async function foo() {
    let bar: CoreResponse = await core.events.request({
        channel: "PLUGIN-CHANNEL",
        method: "doStuff",
        param1: "bam",
    })
}
```

Requests return a Promise of the type `CoreResponse`. Notifications don't return anything.
