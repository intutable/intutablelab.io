**Intutable** is a data wrangling tool created by the
[Parallel and Distributed Systems Group](https://pvs.ifi.uni-heidelberg.de/home)
at the Institute of Computer Science of Heidelberg University.

On this page you can find the documentation, including an architecture overview, plugin development and workflow description.  
If you want to take a look at the code, head to the [intutable group](https://gitlab.com/intutable) at GitLab.